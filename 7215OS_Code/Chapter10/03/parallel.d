import std.parallelism;
import std.math;

// just to populate the initial array
int[] generateNumbers() {
  int[] a;
  a.length = 50000;
  foreach(idx, ref i; a)
    i = cast(int) idx; // cast needed for 64 bit compiles
  return a;
}

static int[] numbers = generateNumbers();

void main() {
  foreach(i; 0 .. 1000) // run several times for benchmarking
//  foreach(ref num; numbers) // linear loop
  foreach(ref num; parallel(numbers)) // parallel loop
    num *= sqrt(abs(sin(num) / cos(num))); // arbitrary per-item work

  import std.stdio;
  writeln(numbers[0 .. 20]); // display some results
}

