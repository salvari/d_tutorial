import core.sys.windows.windows; // basic Windows headers
import std.conv;

 // Not all necessary functions are defined in core.sys.windows.windows
 // but that’s never a dealbreaker: we can just define the prototypes ourselves
// ReadFileEx is specialized for asynchronous reading
  extern(Windows)
    BOOL ReadFileEx(HANDLE, LPVOID, DWORD, OVERLAPPED*, void*);
// SleepEx will pause the program, allowing our async handler to be called when the data is ready
  extern(Windows)
    DWORD SleepEx(DWORD, BOOL);

// This function will be called when the operation is complete
extern(Windows) void readCallback(DWORD errorCode, DWORD numberOfBytes, OVERLAPPED* overlapped) {
  // hEvent carries user-defined data. We load it with a pointer to the buffer below.
  auto data = (cast(ubyte*) overlapped.hEvent)[0 .. numberOfBytes];
  import std.stdio;
  writeln("Received: ", cast(string) data);
  // we may issue another read here to get more data
}

void main() {
   // We need to open the file with the Windows API too, so we can set the
  // required FILE_FLAG_OVERLAPPED for asynchronous access
  auto handle = CreateFileA("test.txt", GENERIC_READ, 0, null, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, null);
  if(handle is null) throw new Exception("Couldn't open file.");
  scope(exit) CloseHandle(handle); // always cleanup C resources with scope guards

  OVERLAPPED overlapped;
  ubyte[1024] buffer;
  overlapped.hEvent = buffer.ptr; // This is allowed to be user-defined data, we’ll fill it with a pointer to the buffer for use in the callback
  // issue the async read request
  if(ReadFileEx(handle, buffer.ptr, buffer.length, &overlapped, &readCallback) == 0)
    throw new Exception("ReadFileEx failed " ~ to!string(GetLastError()));

  /* Do other things while the operating system loads the data for us…. */

  SleepEx(500, true); // wait for the operation to complete
}

