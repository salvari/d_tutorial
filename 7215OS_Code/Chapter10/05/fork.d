import core.sys.posix.unistd;
import core.sys.posix.sys.wait;

void main() {
  if(auto pid = fork()) {
    if(pid < 0) throw new Exception(“fork failed”);
    // this is the parent, continue doing
    // work here
    int statusCode;
    wait(&statusCode); // wait for the child to finish
  } else {
    // this is the child, it can do independent work
    // here
  }
}
