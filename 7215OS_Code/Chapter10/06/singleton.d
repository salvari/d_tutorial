class ThreadLocalSingleton {
  private static bool initialized;
  private static shared(ThreadLocalSingleton) instance;

  static shared(ThreadLocalSingleton) getInstance() {
    if(!initialized) {
      synchronized(ThreadLocalSingleton.classinfo) {
        if(instance is null) {
          instance = new shared ThreadLocalSingleton();
        }
      }
      initialized = true;
    }
    return instance;
  }

  int count = 0;
  void foo() shared {
    import std.stdio;
    count++;
    writeln("foo ", count);
  }
}

void useSingleton(shared(ThreadLocalSingleton) s) {
  s.foo();
}

void main() {
  auto i = ThreadLocalSingleton.getInstance();
  useSingleton(i);

  // you can also spawn new threads that use the singleton
}

