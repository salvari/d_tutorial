import std.concurrency;

struct MyMessage {
  int a;
}

void writeToFile(string filename, string text) {
  import std.stdio;
  auto file = File(filename, "wt");
  file.write(text);
}

void doSomething(Tid parent, Duration sleepDuration) {
  import core.thread; // for sleep
  Thread.sleep(sleepDuration);
  send(parent, MyMessage(sleepDuration));
}

void main() {
  // writing to files can be slow, let’s spawn a thread
  // so the rest of the program doesn’t have to wait
  spawn(&writeToFile, "text.txt", "writing some text");

  // we’ll also make another worker thread to do a slow
  // operation
  auto tid = spawn(&doSomething, thisTid, 3.seconds);

  // we’ll now wait until the child sends us a message
  receive((MyMessage msg) {
    import std.stdio;
    writeln("Got my message: ", msg.a);
  });
}

