import core.thread;

int count;

void testFunction() {
  import std.stdio;
  count++;
  writeln("Fiber run count: ", count);
  Fiber.yield();
  writeln("Fiber recalled after yielding!");
}

void main() {
  auto fiber1 = new Fiber(&testFunction);
  auto fiber2 = new Fiber(&testFunction);

  // Call the fiber, this will return when it calls Fiber.yield
  fiber1.call();
  fiber2.call();

  // now it will resume execution from the last yield call
  fiber1.call();
  fiber2.call();
}

