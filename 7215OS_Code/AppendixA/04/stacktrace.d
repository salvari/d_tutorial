string getStackTrace() {
  import core.runtime;

  version(Posix) {
    // druntime cuts out the first few functions on the trace because they are internal
    // so we'll make some dummy functions here so our actual info doesn't get cut
    Throwable.TraceInfo f4() { return defaultTraceHandler(); }
    Throwable.TraceInfo f3() { return f4(); }
    Throwable.TraceInfo f2() { return f3(); }
    Throwable.TraceInfo f1() { return f2(); }
    auto strace = f1();
  } else {
    auto trace = defaultTraceHandler();
  }

  return trace.toString();
}

void main() {
	import std.stdio;
	writeln(getStackTrace());
}
