void main() {
	int a = 2^^3; // a == 8
	float b = 2.0 ^^ 3.0; // b == 8.0
	auto c = 2 * 2 ^^ 3; // c == 16
}
