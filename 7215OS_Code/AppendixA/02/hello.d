enum serialPort = cast(shared(uint)*) 0x101f1000;

void printToSerialPort(string s) {
	foreach(c; s)
		*serialPort = c;
}

void main() {
	printToSerialPort("Hello world from D!\n");
}

