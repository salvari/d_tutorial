.global _start
_start:
LDR sp, =stack_top  @ prepare a stack
BL _d_run_main      @ call our D entry point
B .                 @ endlessly loop

