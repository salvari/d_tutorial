void main(){
  string title = "The D Programming Language: Easy Speed!";
  import std.uni, std.string, std.conv, std.range,std.regex;
  title = normalize(title);
  title = title.toLower();
  title = std.regex.replaceAll(title, regex(`[^a-z0-9 \-]`), "");
  title = title.squeeze(" ")
   .replace(" ", "-")
   .take(32).to!string;
  import std.stdio;
  writeln("The title is: ", title);
}
