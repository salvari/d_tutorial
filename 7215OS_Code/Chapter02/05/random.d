import std.random, std.conv, std.string, std.stdio;

int playRound(ref Random generator, File userInput, File saveFile) {
    int tries = 0;
    auto value = uniform(0, 100, generator);

    writeln("Guess the number between 0 and 100:");
    while(true) {
        tries++;
        auto guess = userInput.readln().strip().to!int;;;
        if(saveFile.isOpen) saveFile.writeln(guess); // save in the replay file
        if(guess == value) break; // correct!
       writefln("Your guess of %s was too %s, please try again,", guess, guess > value ? "high" : "low");
    }
    writefln("Correct! You guessed %d in %d tries.", value, tries);
     return tries;
  }
void main(string[] args) {
    Random gen;
    File userInput, saveFile;
    // prepare input and seed the generator
     if(args.length > 1) {
         // use the given replay file
        userInput = File(args[1], "rt");
        gen.seed(to!uint(userInput.readln().strip())); //// using the saved seed = same game
    } else {
        // new game, use an unpredictable seed and create a replay file
       userInput = stdin; // take input from the keyboard
       auto seed = unpredictableSeed;; // a random game
       gen.seed(seed);
       saveFile = File("replay.txt", "wt");
      saveFile.writeln(seed); // save the seed so we can reproduce the game later
    }
   int totalTries = 0;
   foreach(round; 0 .. 3)
       totalTries += playRound(gen, userInput, saveFile);
   writefln("You guessed three numbers in %s tries!", totalTries);
}

