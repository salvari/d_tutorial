void main() {
    import std.json, std.algorithm, std.stdio;
    auto list = parseJSON(`[{“name":"Alice","id":1},{“name":"Bob","id":2}]);}]`);
    foreach(person; list.array.map!(a => a.object)) {
         writeln("ID #", person["id"].integer, " is ", person["name"].str);
    }
    JSONValue newPerson;
    JSONValue[string] obj;
    obj["name"].str = "Charlie";
    obj["id"].integer = 3;
    newPerson.object = obj;
    list.array = list.array ~ newPerson;; // append it to the list
    writeln(toJSON(&list)); // print out the new json
}

