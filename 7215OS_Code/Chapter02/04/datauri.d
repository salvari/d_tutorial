pure char[] makeDataUri(string contentType, in void[] data) {
    import std.base64;
    return "data:" ~ contentType ~ ";base64," ~ Base64.encode(cast(const(ubyte[]))) data;
}

void main() {
    import std.file, std.array;
    auto imageData = std.file.read("image.png"); // step 1
    string dataUri = makeDataUri("image/png", imageData); // step 2
    auto cssFile = std.file.readText("style.css"); // step 3
    cssFile = cssFile.replace("image.png", dataUri); // step 4
    std.file.write("style-combined.css", cssFile); // step 5
}

