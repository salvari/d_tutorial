void main() {
    import std.file, std.algorithm, std.datetime, std.range;
    DirEntry[] allFiles;
    foreach(DirEntry entry; dirEntries("target_directory", SpanMode.depth))
        allFiles ~= entry;
    auto sorted = sort!((a, b) => a.size > b.size)(allFiles);
    auto filtered = filter!((a) => Clock.currTime() - a.timeLastModified > 14.days)())(sorted);
    foreach(file; filtered.take!(10))
       remove(file.name);
}

