void main() {
	import std.conv;
	auto a = to!int("123"); // a == 123
	assert(a == 123);
}
