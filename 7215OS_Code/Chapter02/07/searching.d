void main(string[] args) {
	import std.regex, std.stdio;
	auto re = regex(args[1], "g");
	foreach(line; stdin.byLine)
		if(line.matchAll(re)) writeln(line, " was a match!");
}

