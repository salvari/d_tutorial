// our linked list structure
struct ListNode {
    int value;
    ListNode* next;
}
// now, we’ll implement the range
// separating correct from incorrect functions
import std.range;
struct ListWalkerRange(bool beSlow) {
    // these are all appropriate for linked lists
    // notice how they have simple implementations with no loops
    ListNode* current;
    this(ListNode* a) { current = a; }
    @property bool empty() { return current is null; }
    // note: this returns ref to be assignable. we’ll see why soon
    @property ref int front() { return current.value; }
    void popFront() { current = current.next; }
    @property ListWalkerRange save() { return this; }

    // and now, the inappropriate functions
    // they are in a static if so we can turn them on and off
    // these emulations are VERY slow – they all depend on loops!
    static if(beSlow) {
        ListNode* currentFromBack;
        bool backInitialized = false;
        @property ref int back() {
            // we could initialize in the constructor, but to
            // keep fast from slow completely separate, we’ll
            // do it here instead
            if(!backInitialized) {
                popBack();
                backInitialized = true;
            }

            return currentFromBack.value;
        }

        // we'll use this to limit the size of our slice
        size_t maxLength = size_t.max;

        void popBack() {
            // to find the back on a singly linked list,
            // we have to start over from the beginning!
            auto where = this.current;
            auto length = 0;
            while(where !is currentFromBack && length<maxLength) {
                currentFromBack = where;
                where = where.next;
                length++;
            }
        }

        // random access also requires a length property
        // again, with a linked list, this means looping!
        @property size_t length() {
            size_t count = 0;
            auto where = current;
            while(where && count < maxLength) {
                count++;
                where = where.next;
            }
            return count;
        }

        ref int opIndex(size_t idx) {
            auto where = this.current;
            foreach(i; 0 .. idx)
                where = where.next;
            return where.value;
        }

        // we’ll also implement slicing, as required by sort
        ListWalkerRange!true opSlice(size_t start, size_t end) {
            auto where = this.current;
            foreach(i; 0 .. start)
                where = where.next;

            auto range = ListWalkerRange!true(where);
            range.maxLength = end - start;
            return range;

        }
    }
}

// assert the fast walker is a forward range…
static assert(isForwardRange!(ListWalkerRange!false));
// but is NOT a random access range!
static assert(!isRandomAccessRange!(ListWalkerRange!false));

// and assert the slow one does properly implement the interface
// (though not efficiently!)
static assert(isRandomAccessRange!(ListWalkerRange!true));

// and these accessor functions make our range from a node.
ListWalkerRange!false walker(ListNode* node) {
    return ListWalkerRange!false(node);
}
ListWalkerRange!true slowWalker(ListNode* node) {
    return ListWalkerRange!true(node);
}

void main() {
    import std.stdio;

    ListNode* list = new ListNode(0);
    auto current = list;
    foreach(i; 1 .. 1000) {
        // feel free to randomize the value if you want,
        // we’ll be sorting it later anyway!
        current.next = new ListNode(i);
        current = current.next;
    }


    auto result = benchmark!(
        {    auto sorted = sort(list.walker.array);    },
        {    auto sorted = sort(list.slowWalker);      }
    )(100);
    writefln("Emulation resulted in a sort that was %d times slower.",
        result[1].hnsecs / result[0].hnsecs);

}


