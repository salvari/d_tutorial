struct User {
    int id;
    string name;
    string title;
}

void main() {
	User[] users;
	users ~= User(1, "Alice", "President");
	users ~= User(2, "Bob", "Manager");
	users ~= User(3, "Claire", "Programmer");


	import std.algorithm;
	import std.range;
	import std.typecons : tuple; // we use this below
	auto resultSet = users.
	    sort!((a, b) => a.id > b.id). // the ORDER BY clause
	    filter!((item) => item.name.startsWith("A")). // the WHERE clause
	    take(5).
	    map!((item) => tuple(item.id, item.name, "Title: " ~ item.title)); // the field list and transformations
	import std.stdio;
	foreach(line; resultSet)
	    writeln(line[0], " ", line[1], " ", line[2]);

}
