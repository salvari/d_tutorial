import std.range, std.traits;
auto average(Range)(Range range)
if(isInputRange!Range && !isInfinite!Range && isNumeric!(ElementType!Range))
{
     static if(isFloatingPoint!(ElementType!Range))
        double sum = 0;
     else
        long sum = 0;
     int count = 0;
     foreach(item; range) {
        count++;
        sum += item;
     }
     return sum / count;
}

void main() {
	writeln(average([1, 2, 3])); // prints 2
}
