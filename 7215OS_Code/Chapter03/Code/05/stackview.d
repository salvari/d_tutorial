struct StackView(T) {
     // we’ll start with the data members and constructors that peek into the collection
    private const(T)[] data; // our read-only view into the data
    private this(const(T)[] data) {
        this.data = data;
    }
    // and our range primitives
    @property bool empty() const {
           return data.length == 0;
    }
    @property const(T) front() const {
          return data[$ - 1]; // the stack pops backwards in the array
     }
     void popFront() {
         data = data[0 .. $ - 1];
     }
}
@property auto view() const {
    return StackView!T(buffer[0 .. currentPosition]);
}

