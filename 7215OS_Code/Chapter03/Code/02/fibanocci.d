import std.range;
struct FibonacciRange {
    // input range required methods
    // Fibonacci is an infinite sequence, so it is never empty
    enum bool empty = false;
    @property int front() { return current; }
    void popFront() {
        previous = current;
        current = next;
        next = current + previous;
    }
    // data members for state between calls
    private {
        int previous = 0;
        int current = 0;
        int next = 1;
    }
    // other range functions we can implement cheaply
    @property FibonacciRange save() { return this; }
}
// explicit check that it fulfils the interface we want
static assert(isForwardRange!FibonacciRange);

// helper function to create the range
FibonacciRange fibonacci() {
     return FibonacciRange();
}

void main() {
    import std.stdio;
    auto numbers = fibonacci();
    writeln(numbers.take(15));
}

