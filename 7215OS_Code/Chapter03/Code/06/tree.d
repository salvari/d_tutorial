import stack;

struct TreePart {
    string payload;
    TreePart[] children;
}
void walkTree(void delegate(string) visitor, TreePart root) {
    visitor(root.payload);
    foreach(child; root.children)
        walkTree(visitor, child);
}

struct TreeVisitor { // step one, the skeleton
    // steps two and three, struct with our variables
    struct Position {
        TreePart root;
        int childPosition;
    }

    // step four, create the required locals and stack
    Stack!(Position) stack;
    Position current;

    // step five, constructor
    this(TreePart root) {
        current.root = root;
        // we use childPosition == -1 to indicate that
        // we haven’t entered the child loop yet
        current.childPosition = -1;
    }

    // step six
    @property string front() {
        return current.root.payload;
    }

    // step seven
    @property bool empty() {
        return
         // if we’re at the end of the children…
         current.childPosition + 1 == current.root.children.length
         // …and the stack is empty, we’re done
         && stack.isEmpty;
    }

    // step eight
    void popFront() {
        current.childPosition++; // advance position
        if(current.childPosition == current.root.children.length) {
            // the foreach loop would have just returned, so we
            // start walking back up the tree…
            current = stack.pop();
            // picking up where we left off, if possible
            if(!empty)
                popFront();
        } else {
            // we’re still inside the loop,
            // proceed deeper into the tree
            stack.push(current);
            // and reset our variables for the next part
            current.root = current.root.children[current.childPosition];
            current.childPosition = -1;
        }
    }
}
// step nine
static assert(isInputRange!TreeVisitor);

void main() {
    import std.stdio;

    void visitor(string part) {
        writeln(part);
    }

    // initialize our test tree
    TreePart root;
    root.payload= "one";
    root.children = [TreePart("two", [TreePart("three", null)]), TreePart("three", null)];

    // first, walk it recursively
    walkTree(&visitor, root);

    writeln("****"); // separator for easy viewing

    // then use our range to do the same thing
    foreach(part; TreeVisitor(root))
        visitor(part);
}


