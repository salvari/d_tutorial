struct Stack(T, size_t expectedMaxSize = 32) {
    T[expectedMaxSize] initialBuffer;
    T[] buffer;
    size_t currentPosition;
    bool isEmpty() { return currentPosition == 0; }
    void push(T item) {
        // initialization
        if(buffer is null) buffer = initialBuffer[];
        // growth if needed
        if(currentPosition == buffer.length) buffer.length += 64;
        // the actual push operation
        buffer[currentPosition++] = item;
    }
    T pop() {
        if(currentPosition == 0)
             throw new Exception("Empty stack cannot pop.");
        return buffer[--currentPosition];
    }
}

unittest {
	Stack!int stack;
	stack.push(1);
	stack.push(2);
	assert(stack.pop() == 2);
	assert(stack.pop() == 1);
	assert(stack.isEmpty()); // passes, we’ve used all the data
}
