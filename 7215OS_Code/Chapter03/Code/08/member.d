void main() {
	import std.range;
	InputRange!int rangeObj;
	rangeObj = inputRangeObject([1,2,3]);
}

class Example {
	import std.algorithm;
	typeof(filter!((a) > 0)([1,2,3])) filteredRange; // declare it
	this() {
	    filteredRange = filter!((a) > 0)([1, 2, 3]); // initialize it
	}
}

