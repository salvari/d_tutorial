void main() {
    import std.stdio;

    ListNode* list = new ListNode(0);
    auto current = list;
    foreach(i; 1 .. 1000) {
        // feel free to randomize the value if you want,
        // we'll be sorting it later anyway!
        current.next = new ListNode(i);
        current = current.next;
    }
}
