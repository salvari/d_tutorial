import std.range;
import std.stdio;
struct HexDumper {
    int outputted;
    void put(in void[] data) {
        foreach(b; cast(const ubyte[]) data) {
             writef("%02x ", b);
             outputted++;
             if(outputted == 16) {
                 writeln();
              outputted = 0;
          }
      }
    }
    ~this() {
        if(outputted)
            writeln();
      }
}
static assert(isOutputRange!(HexDumper, ubyte));
static assert(isOutputRange!(HexDumper, char));
void main() {
    HexDumper output;
    output.put("Hello, world! In hex.");
} 

