void main() {
	import core.sys.posix.unistd; // analogous to #include <unistd.h>
	string hello = "Hello, world!";
	write(1 /* stdout file descriptor */, hello.ptr, hello.length);
}
