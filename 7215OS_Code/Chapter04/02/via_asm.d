void main() {
	string hello = "Hello, world!";
	auto hptr = hello.ptr;
	auto hlength = hello.length;
	version(linux) {
	version(D_InlineAsm_X86)
	asm {
	    mov ECX, hptr;
	    mov EDX, hlength;
	    mov EBX, 1; // stdout
	    mov EAX, 4; // sys_write
	    int 0x80; // perform the operation
	} else version(D_InlineAsm_X86_64)
	asm {
	    mov RSI, hptr;
	    mov RDX, hlength;
	    mov RDI, 1; // stdout
	    mov RAX, 1; // sys_write
	    syscall; // perform the operation
	} else static assert(0, "Unsupported Processor for inline asm");
	} else static assert(0, "Unsupported OS for this function");
}
