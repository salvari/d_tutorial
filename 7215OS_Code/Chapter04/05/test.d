struct Class {
    extern(C++) interface Vtable {
          void add(int a);
    }
     void* vtbl;
     @property Vtable getVirtuals() {  return cast(Vtable) &this; }
     alias getVirtuals this;
     extern(C++) void print();
     int number;
    pragma(mangle, “_ZN5ClassC2Ei”)
    extern(C++) void ctor(int a);
    this(int a) { ctor(a); }
}
void main() {
    Class c = Class(0);
    c.num = 10;
    c.add(10);
    c.print();
}

