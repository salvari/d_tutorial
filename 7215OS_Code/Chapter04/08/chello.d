
import win32.winuser;

import comhelpers;
import ihello;

@ComGuid(CLSID_Hello)
class CHello : IHello, IDispatch {
        mixin ComObject!();
        mixin IDispatchImpl!();

        extern(Windows)
        public override HRESULT Print() {
                import std.stdio; writeln("cool?");
                MessageBoxA(null, "CHello.Print()", null, MB_OK);
                return NOERROR;
        }
}

mixin ComServerMain!(CHello, "Hello", "1.0");

