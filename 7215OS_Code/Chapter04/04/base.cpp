#include<stdio.h>
#include<stdlib.h>

// our C++ class
class Animal {
public:
	// ideally, we would prefer to use classes
	// with no member variables and no additional
	// functions. D and C++ don't understand each
	// other's constructors and destructors and will
   // have different ideas as to where members will be found.

	// But, perfection may not be realistic with
	// existing code, so we'll see how to possibly work with it.
	int member; // we must not use this on a D object *at all*

	virtual ~Animal() {}

	// this is what we really want: abstract virtual
	// functions we can implement with an interface.
	virtual void speak() = 0;
};

// A concrete C++ class we'll use in D via the interface
class Dog : public Animal {
	void speak() {
		printf("Woof\n");
	}
};

// OUr D functions
extern "C++" void useAnimal(Animal* t);
extern "C++" Animal* getCat();
extern "C++" void freeCat(Animal* cat);

// D Runtime functions from the library
extern "C" int rt_init();
extern "C" void rt_term();

// RAII struct for D runtime initialization and termination
struct DRuntime {
	DRuntime() {
		if(!rt_init()) {
			// you could also use an exception
			fprintf(stderr, "D Initialization failed");
			exit(1);
		}
	}
	~DRuntime() {
		rt_term();
	}
};

void main() {
	// be sure to initialize the D runtime before using it
	DRuntime druntime;

	Dog dog;
	// use a C++ class in a D function
	useAnimal(&dog);

	// use a D class from C++
   // you may use a smart pointer or RAII struct here too
   // so the resource is managed automatically.
	Animal* cat = getCat(); // use a factory function
	cat->speak(); // call the function!
   // it was created in D, so it needs to be destroyed by D too
	freeCat(cat);
}

