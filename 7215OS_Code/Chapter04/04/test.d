import std.stdio;
import core.stdc.stdlib; // for malloc

extern(C++)
interface Animal {
   // since the C++ class had a virtual destructor
   // we must define an entry, but we don’t want to use it
	void _destructorDoNotUse();
	void speak();
}

class Cat : Animal {
   // Note that we did *not* copy the C++ member variable.
   // Doing so would be futile; the layouts will not match.
   // Creating D child classes of a C++ class with member 
   // variables should be avoided if at all possible.

	extern(C++)
	void _destructorDoNotUse() {}

	extern(C++)
	void speak() { try {writeln("Meow!");}catch(Throwable) {} }
}

// We’ll implement a factory function for getting Cats
extern(C++)
Animal getCat() {
   // Manage the memory with malloc and free for full control
	import std.conv;
	enum size = __traits(classInstanceSize, Cat);
	auto memory = malloc(size)[0 .. size];
	return emplace!Cat(memory);
}

extern(C++)
void freeCat(Animal animal) {
	auto cat = cast(Cat) animal;
	if(cat !is null) {
		destroy(cat);
		free(cast(void*) cat);
	}
}

// This can also use an object from C++
extern(C++)
void useAnimal(Animal t) {
	t.speak();
}

