struct ScriptEngine { // step 1
    import arsd.script;

    this(string scriptSource) { // step 2
        scriptGlobals = var.emptyObject;

        // add a function, transforming functions and returns
        import std.stdio;
        scriptGlobals.write = delegate var(var _this, var[] args) {
            writeln(args[0]);
            return var(null);
        };

        // run the user's script code (step 4)
        interpret(scriptSource, scriptGlobals);
    }

    var scriptGlobals;

    auto opDispatch(string func, T...)(T t) { // step 3
        if(func !in scriptGlobals)
            throw new Exception("method \"" ~ func ~ "\" not found in script");
        var[] args;
        foreach(arg; t)
            args ~= var(arg);
        return scriptGlobals[func].apply(scriptGlobals, args);
    }
}

void main() {
    // step 5
    auto scriptContext = ScriptEngine(q{
         // this is script source code!
        function hello(a) {
            // call the D function with the argument
            write("Hello, " ~ a ~ "!");

            // and return a value too
            return "We successfully said hello.";
        }
    });

    // call a script function and print out the result
    import std.stdio;
    writeln("The script returned: ", scriptContext.hello("user"));
}

