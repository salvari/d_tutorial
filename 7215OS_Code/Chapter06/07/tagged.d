struct Dynamic {
	enum Type { integer, string }
	private Type type;
	private union {
		int integer;
		string str;
	}

	this(T)(T t) { // easy construction
		this = t;
	}

	// assignment of our supported types
	Dynamic opAssign(inti) {
		type = Type.integer;
		integer = i;
		return this;
	}

	Dynamic opAssign(string i) {
		type = Type.string;
		str = i;
		return this;
	}

	// getting our types back out into static variables
	// here, we used the coercion option instead of throwing if
	// we had the wrong type
	int asInteger() {
		final switch(type) {
			case Type.integer:
				return integer;
			break;
			case Type.string:
				importstd.conv;
			return to!int(str);
			break;
		}
	}

	string asString() {
		final switch(type) {
			case Type.string:
				returnstr;
			break;
			case Type.integer:
				importstd.conv;
			return to!string(integer);
			break;
		}
	}

	// we may also implement other operators for more integration
}

void main() {
	// test usage
	Dynamic a = 10; // calls the constructor
	a = "20"; // calls opAssign
	import std.stdio;
	writeln(a.asInteger());
}

