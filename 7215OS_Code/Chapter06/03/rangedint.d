struct RangedInt(int min, int max) {
    private int _payload;

    @disable this();

    this(int value) {
        _payload = value;
        check();
    }

    RangedInt check () {
        assert(_payload >= min);
        assert(_payload < max);
        return this;
    }

    RangedInt opUnary(string op)() {
        RangedInt tmp = _payload;
        mixin("tmp= " ~ op ~ "_payload;");
        return tmp.check();
    }

    RangedInt opBinary(string op)(int rhs) {
        mixin("return RangedInt(_payload " ~ op ~ "rhs);");
    }

    RangedInt opOpAssign(string op)(intrhs) {
        mixin("_payload " ~ op ~ "= rhs;");
        return check();
    }

    RangedInt opAssign(intrhs) {
        _payload = rhs;
        return check();
    }

    string toString() {
        import std.conv;
        return to!string(_payload);
    }

    @property int getPayload() { return _payload; }

    alias getPayload this;
}

void main() {
    import std.stdio;
    RangedInt!(50, 100) r = 80;
    r += 19;
    r++; // this will trigger an assertion failure
}

