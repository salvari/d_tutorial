struct x86Accumulator {
	union {
		long RAX;
		// lowest items first because x86 is little endian
		struct {
			union {
				struct {
					union {
						struct {
							byte AL;
							byte AH;
						}
						short AX;
					}
					short upperWord;
				}
				int EAX;
			}
			int upperDWord;
		}
	}
}

void main() {
    x86Accumulator a;
    import std.stdio;
    a.EAX = 50;
    // set the high byte of the low word independently
    // equivalent to adding 256...
    a.AH = 1;
    writeln(a.RAX); // should be 306
}

