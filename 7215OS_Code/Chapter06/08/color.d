struct Color {
    union {
        ubyte[4] rgba;
        struct {
            ubyte r;
            ubyte g;
            ubyte b;
            ubyte a;
         }
    }
}

void main() {
	Color c;
	c.r = 100; // accessing the inner name is easy
	assert(c.rgba[0] == 100); // alternative view into same data
}
