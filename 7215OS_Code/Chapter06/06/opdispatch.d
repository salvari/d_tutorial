class Foo {
    int test() { return 0; }
    void nop() {}
}

struct Wrapped {
        Foo f;
        this(Foo f) { this.f = f; }
        auto opDispatch(string s, T...)(T t) {
            return mixin("f." ~ s ~ "(t)");
        }
}

void main() {
        Wrapped w = new Foo();
        Int i = w.test();
        w.nop();
}

