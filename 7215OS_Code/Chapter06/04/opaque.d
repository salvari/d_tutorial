private struct OpaqueImpl{} // step 1
alias OpaqueImpl* OpaqueHandle; // step 2, pointer method
// or step 2, struct method
// structOpaqueHandle{ privateOpaqueImpl* item; }
// interface functions
OpaqueHandle createObject();
void freeObject(OpaqueHandleimpl);
void add(OpaqueHandleimpl, inti);
void print(OpaqueHandleimpl);
void main() {
    // usage:
    auto handle = createObject();
    scope(exit) freeObject(handle);
    handle.add(10); // call using UFCS
    handle.print();
}

