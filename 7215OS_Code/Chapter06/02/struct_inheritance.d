struct Base {
    int x;
    int y;
}
struct Derived {
    int dxy ;
    int dy;
    Base base; // a member of the base type - composition
    alias base this; // then use alias this for subtyping
}
void operateOnBase(Base base) {}
void operateOnDerived(Derived derived) {}

void main() {
    Derived d;
    operateOnBase(d); // works
}
