struct TranslatedString {
    private string value;
        @property string getValue() { return value; }
    alias getValue this;
}

private TranslatedString localize(string key, bool requireResult = true) {
        // we’ll use an associative array for the translation
        // it would also be possible to load from a file, at
        // compile time
        enum string[string] translations =
                [
                        "Hello!" : "¡Hola!"
                ];

        if(auto value = key in translations)
          return TranslatedString(*value);
        if(requireResult)
            throw new Exception(key ~ " is not translated");
       return TranslatedString(null);
}

template T(string key, string file = __FILE__, size_t line = __LINE__) {
    version(gettext) {
            // prints messages at compile time with all strings
            // in the program, if requested
            import std.conv;
            pragma(msg, "#: " ~ file ~ ":" ~ to!string(line));
            pragma(msg, "msgid \"" ~ key ~ "\"");
            pragma(msg, "msgstr \""~localize(key, false)~"\"");
            pragma(msg, "");
        }

     enum T = localize(key);
}

void main() {
    auto p = T!"Hello!"; // translated string literal
    import std.stdio;
    writeln(p);
}

