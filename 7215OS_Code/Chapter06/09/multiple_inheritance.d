import std.stdio : writeln;
interface Base1 {
    void foo();
}

mixin template Base1_Impl() {
  void foo() { writeln("default foo impl"); }
}

interface Base2 {
  void bar();
  void baz();
}

mixin template Base2_Impl() {
  int member;
  void bar() { writeln("default bar impl"); }
  void baz() { writeln("default bazimpl"); }
}

classMyObject : Base1, Base2 {
  mixin Base1_Impl!();
  mixin Base2_Impl!();

  void baz() {
    writeln("custom bazimpl");
    }
}

void main() {
  MyObjectobj = new MyObject();

    Base1 base1 = obj; // works on all interfaces
    Base2 base2 = obj; // works on all interfaces

  obj.foo(); // call methods through object
  obj.baz(); // note that the custom one is called
  base2.bar(); // or through interface
  base2.baz();
}

