interface Example {
    int negate(int a)
       in { assert(a >= 0); }
       out(ret) { assert(ret <= 0); }
        // note: no body since this is an interface
}
class ExampleImplementation : Example {
    int negate(int a)
      in { assert(a >= 0); } // same input restriction
      body { // note the body keyword following the contracts
       return –a;
    }
    invariant() {
      // we have no data, so no need for any invariant assertions
    }
}
void main() {
    auto e = new ExampleImplementation();
    e.negate(-1); // throws an assertion failure
}

