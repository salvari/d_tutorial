bool checkInputRange(T)() {
    if(!__ctfe) {
       // notice that this portion is the same as std.range.isInputRange
        T t = void;
        if(t.empty) {}
        t.popFront();
        auto f = t.front;
    }
    return true;
}
template isInputRange(T) {
    enum isInputRange = is(typeof(checkInputRange!T));
}
struct Test {
    int[] s;
    bool eempty() { return s.length == 0; } // typo intentional!
    int front() { return s[0]; }
    void popFront() { s = s[1 .. $]; }
}
// this tells it failed, but not why…
//static assert(isInputRange!Test);

// the error message here points out the typo!
static assert(checkInputRange!Test);

