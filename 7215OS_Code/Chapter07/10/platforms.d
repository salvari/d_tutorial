version(Windows)
void writeToFile(HANDLE file, in void[] data) {
    if(!WriteFile(file, data.ptr, data.length, null, null))
       throw new Exception(“WriteFile failed”);
}
else version(Posix)
void writeToFile(int file, in void[] data) {
     if(write(file, data.ptr, data.length) < 0))
        throw new Exception(“write failed”);
}
else static assert(0, "Unsupported platform");

