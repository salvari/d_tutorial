class CachingObject {
    private int cache;
    private bool cacheSet;
    int getExpensiveComputation() {
          if(!cacheSet)
            cache = 31415927; // suppose this was an expensive calculation…
         return cache;
    }
}

