class ConditionalCaching {
    // support mutable construction
    this() {}

    // support immutable construction
    this() immutable {
        // we’ll eagerly cache when immutable
        // since lazy caching is completely impossible
        cache = performExpensiveComputation();
        cacheFilled = true;
    }

    // our cache...
    private int cache;
    private bool cacheFilled;

    // helper function to perform the computation
    // this must be refactored into a const method to be
    // usable from all other methods.
    private int performExpensiveComputation() const {
        return 314159265;
    }

    // const getter: use the cache if we can, but if it isn't
    // already filled, we can't fill it now, so just perform the
    // computation now.
    int getExpensiveComputation() const {
        if(cacheFilled)
            return cache;
        else
            return performExpensiveComputation();
    }

    // mutable getter: we can fill in the cache, so we will.
    int getExpensiveComputation() {
        if(!cacheFilled) {
            cache = performExpensiveComputation();
            cacheFilled = true;
        }
        return cache;
    }
}

void main() {
// create a mutable object
    ConditionalCaching c = new ConditionalCaching();
    // this won't compile: we have to construct an immutable
    // instance specially
    // immutable(ConditionalCaching) i = new ConditionalCaching();

    // create an immutable object properly. This calls the
    // immutable constructor
    immutable(ConditionalCaching) d = new immutable ConditionalCaching();

}
