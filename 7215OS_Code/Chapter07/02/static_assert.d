align(1) struct IDTLocation {
   align(1):
   ushort length;
   uint offset;
}
static assert(IDTLocation.sizeof == 6, "An IDTLocation must be exactly six bytes with no padding.");

