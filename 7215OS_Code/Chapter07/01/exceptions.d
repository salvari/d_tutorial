struct MyRange {
   int current = 0;
   @property bool empty() { return current < 10; }
   @property int front() {
      // it is a programming error to call front on an empty range
      // we assume current is valid, so we’ll verify with assert.
      assert(!empty);
      return current;
   }
   void popFront() {
       // it is always a bug to call popFront on an empty range
      assert(!empty);
      current++;
    }
}
void appendToFile() {
    import core.stdc.stdio;
    auto file = fopen("file.txt", "at");
    scope(exit) fclose(file);
    // Files not opening is not a bug – it is an outside condition
    // so we will throw an exception instead of asserting.
    if(file is null)
         throw new Exception("Could not open file");
    fprintf(file, "Hello!\n");
}

