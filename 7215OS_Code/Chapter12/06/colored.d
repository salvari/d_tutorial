import terminal;

void main() {
  auto terminal = Terminal(ConsoleOutputType.linear);
  terminal.color(Color.green, Color.red);
  terminal.writeln("Hello, world!");
}
