import arsd.mysql;
void main() {
  auto db = new MySql("localhost", "demo_user", "test", "demonstration");
  // Add some data to the table
  db.query("INSERT INTO info (name) VALUES (?)", "My Name");
  db.query("INSERT INTO info (name, age) VALUES (?, ?)", "Other", 5);

  // get the data back out
  foreach(line; db.query("SELECT id, name, age FROM info")) {
    import std.stdio;
    // notice indexing by string or number together
    writefln("ID %s %s (Age %s)", line["id"], line[1], line["age"]);
  }
}

