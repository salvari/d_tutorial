import arsd.web;
class Test : ApiProvider {
  export int add(int a, int b) { return a+b; }
  export string sayHello(string name) {
    return "Hello, " ~ name;
  }
}
mixin FancyMain!Test;

