import arsd.cgi;

import std.array, std.conv;

// This function does a quick and dirty html entity encoding
// to prevent XSS. Typically, I use htmlEntitiesEncode in dom.d
string simpleHtmlEncode(string s) {
  return s.replace("&", "&amp;").
    replace("<", "&lt;").replace(">", "&gt;");
}

void handler(Cgi cgi) {
  // this is the default, so the following line is not
  // necessary. It is here for demonstration purposes.
  cgi.setResponseContentType("text/html; charset=UTF-8");

  string data = "Hello, " ~
    cgi.request("name", "user").simpleHtmlEncode();
  data ~= "<br />\n";
  data ~= "Called from: " ~ cgi.pathInfo.simpleHtmlEncode();
  data ~= "<br />\n";
  data ~= to!string(cgi.get).simpleHtmlEncode() ~ "\n";

  cgi.write(data, true); // all written at once
}

mixin GenericMain!handler;

