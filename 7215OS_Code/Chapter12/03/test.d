import arsd.dom;

void main() {
  auto document = new Document();

  // The example document will be defined inline here
  // We could also load the string from a file with
  // std.file.readText or the web with std.net.curl.get
  document.parseGarbage(`<html><head>
    <meta name="author" content="Adam D. Ruppe">
    <title>Test Document</title>
  </head>
  <body>
    <p>This is the first paragraph of our <a href="test.html">test document</a>.
    <p>This second paragraph also has a <a href="test2.html">link</a>.
    <p id="custom-paragraph">Old text</p>
  </body>
  </html>`);

  import std.stdio;
  // retrieve and print some meta information
  writeln(document.title);
  writeln(document.getMeta("author"));
  // show a paragraph’s text
  writeln(document.requireSelector("p").innerText);
  // modify all links
  document["a[href]"].setValue("source", "your-site");
  // change some html
  document.requireElementById("custom-paragraph").innerHTML = "New <b>HTML</b>!";
  // show the new document
  writeln(document.toString());
}

