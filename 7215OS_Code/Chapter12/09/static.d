import simpledisplay;

void main() {
  bool paused;
  auto image = new Image(256, 256);
  auto window = new SimpleWindow(image.width, image.height);
  window.eventLoop(25,
    () {
      if(paused) return;
      import std.random;
      foreach(y; 0 .. image.height)
      foreach(x; 0 .. image.width)
        image.putPixel(x, y, uniform(0, 2) ? Color.black : Color.white);
      auto painter = window.draw();
      painter.drawImage(Point(0, 0), image);
    },
    (KeyEvent ke) {
      if(ke.key == Key.Space)
        paused = !paused;
      if(ke.key == Key.Escape)
        window.close();
    }
  );
}

