import simpledisplay;

void main() {
  auto window = new SimpleWindow(512, 512, "OpenGL Demo", OpenGlOptions.yes);

  float f = 0.0;
  window.redrawOpenGlScene =  {
    glMatrixMode(GL_PROJECTION);
    glClearDepth(1.0f);
    glEnable (GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glViewport(0,0,window.width,window.height);

    // clear the screen
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);

    glLoadIdentity();
    glRotatef(f, 1, 0, 0);
    f += 4.5;

    glBegin(GL_TRIANGLES);
    // base of the pyramid
    glColor3f(1, 0, 0); glVertex3f(0.5, -0.5, 0);
    glColor3f(0, 1, 0); glVertex3f(0, 0.5, 0);
    glColor3f(0, 0, 1); glVertex3f(-0.5, -0.5, 0);

    // the other three sides connect to the top
    glColor3f(1, 1, 1); glVertex3f(0, 0, 0.5);
    glColor3f(0, 1, 0); glVertex3f(0, 0.5, 0);
    glColor3f(0, 0, 1); glVertex3f(-0.5, -0.5, 0);

    glColor3f(1, 0, 0); glVertex3f(0.5, -0.5, 0);
    glColor3f(1, 1, 1); glVertex3f(0, 0, 0.5);
    glColor3f(0, 0, 1); glVertex3f(-0.5, -0.5, 0);

    glColor3f(1, 1, 1); glVertex3f(0, 0, 0.5);
    glColor3f(1, 0, 0); glVertex3f(0.5, -0.5, 0);
    glColor3f(0, 1, 0); glVertex3f(0, 0.5, 0);

    glEnd();
  };


  window.eventLoop(50,
    delegate () {
      window.redrawOpenGlSceneNow();
    });
}

