import terminal;

void main() {
  auto terminal = Terminal(ConsoleOutputType.linear);
  auto input = RealTimeConsoleInput(&terminal, ConsoleInputFlags.raw);
  terminal.writeln("Press any key to exit");
  auto ch = input.getch();
  terminal.writeln("Bye!");
}

