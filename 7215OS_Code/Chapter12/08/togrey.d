import arsd.png;
void main() {
        auto image = readPng("test.png").getAsTrueColorImage();
        foreach(ref pixel; image.imageData.colors) {
                int average = (pixel.r + pixel.g + pixel.b) / 3;
                pixel.r = pixel.g = pixel.b = cast(ubyte) average;
        }
        writePng("test-bw.png", image);
}

