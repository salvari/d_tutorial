// the UDA we should put on authorized virtuals
enum Virtual;

// alias helper for looping over members
alias helper(alias T) = T;

// Helper function to test for presence of @Virtual
bool isAuthorizedVirtual(alias member)() {
  foreach(attr; __traits(getAttributes, member))
    if(is(attr == Virtual))
      return true;
  return false;
}

// Loop over all members, looking for classes to drill
// into and virtual functions to test.
bool virtualCheck(alias T)() {
  bool passes = true;
  foreach(memberName; __traits(derivedMembers, T)) {
    static if(memberName.length) {
      alias member = helper!(__traits(getMember, T, memberName));
      // drill down into classes
      static if(is(member == class))
        passes = passes && virtualCheck!member;

      // check overloaded functions (if any) for
      // unmarked virtuals
      foreach(overload; __traits(getOverloads, T, memberName))
        static if(__traits(isVirtualMethod, overload)) {
          static if(!isAuthorizedVirtual!overload) {
            // and warn if we find any
            pragma(msg, T.stringof ~ "." ~ memberName
              ~ " " ~ typeof(overload).stringof
              ~ " is virtual");
            passes = false;
          }
        }
    }
  }
  return passes;
}

class Test {
  @Virtual void foo() {} // specifically marked, ok
  void foo(int) {} // virtual but not marked = problem
  final void f() {} // final, ok
}

// We'll use static assert to run the test and cause a compile
// error if any tests fail.
static assert(virtualCheck!(mixin(__MODULE__))); // test all classes in the module

