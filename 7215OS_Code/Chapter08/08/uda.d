struct MyNote { string note; }
@MyNote("this is my note on foo") void foo() {}
MyNote getMyNoteAnnotation(alias F)() {
    foreach(attr; __traits(getAttributes, F)) {
         static if(is(typeof(attr) == MyNote))
             return attr;
    }
    assert(0); // the annotation wasn’t there
}
// get the note and print out the value
// (the member note is of the MyNote struct)
pragma(msg, getMyNoteAnnotation!(foo).note);

