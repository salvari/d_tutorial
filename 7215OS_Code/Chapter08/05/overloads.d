struct S {
  void foo() {}
  void foo(int a) {}
}
void main() {
  import std.stdio;
  foreach(overload; __traits(getOverloads, S, "foo"))
    writeln(typeof(overload).stringof);
}

