
// The functions we want to make available
export {
  @doc("Say hello to the user", ["the user's name to greet"])
    string hello(string name = "user") {
      return "Hello, " ~ name ~ "!";
    }

  @doc("Adds two numbers together.")
    int sum(int a, int b) {
      return a+b;
    }
}

// alias helper for reflection
alias helper(alias T) = T;

// Our user-defined attribute for documentation
struct doc {
  string value;
  string[] argDocs;
}

// Helper function to find a generic value UDA
auto getAttribute(alias mem, T)() {
  foreach(attr; __traits(getAttributes, mem))
    static if(is(typeof(attr) == T))
    return attr;
  return T.init;
}

// Function to do the reflection and call
int runWithArgs()(string[] args) {
  import std.traits, std.conv, std.stdio;

  string name;
  if(args.length) {
    name = args[0];
    args = args[1 .. $];
  }

  // look over the whole module for callable functions
  alias mod = helper!(mixin(__MODULE__));
  foreach(memberName; __traits(allMembers, mod)) {
    alias member = helper!(__traits(getMember, mod, memberName));
    static if(
        // Is it a function marked with export?
        is(typeof(member) == function) &&
        __traits(getProtection, member) == "export")
    {
      if(name == "--help") {
      // user requested help
        if(args.length) {
          if(memberName == args[0]) {
            // print the details of this function
            writef("Usage: %s", memberName);
            foreach(argName; ParameterIdentifierTuple!member)
              writef(" %s", argName);
            writefln("\n\t%s", getAttribute!(member, doc).value);

            if(ParameterTypeTuple!member.length)
              writeln("Arguments:");

            auto argDocs = getAttribute!(member, doc).argDocs;

            foreach(idx, argName; ParameterIdentifierTuple!member) {
              string defaultValue;
              bool hasDefaultValue;
              static if(!is(ParameterDefaultValueTuple!member[idx] == void)) {
                defaultValue = to!string(ParameterDefaultValueTuple!member[idx]);
                hasDefaultValue = true;
              }

              string argDoc = "?";
              if(idx < argDocs.length)
                argDoc = argDocs[idx];

              writefln("\t%s (%s): %s %s",
                  argName,
                  ParameterTypeTuple!member[idx].stringof,
                  argDoc,
                  hasDefaultValue ? "[default=" ~ defaultValue ~ "]" : "");
            }
          }
        } else {
          // no details requested, just show the full listing
          writefln("%16s -- %s", memberName, getAttribute!(member, doc).value);
        }
     // the user did NOT ask for help, call the function if
     // we have the correct name
      } else if(memberName == name) {
        // Prepare arguments
        ParameterTypeTuple!member arguments;
        alias argumentNames = ParameterIdentifierTuple!member;
        alias defaultArguments = ParameterDefaultValueTuple!member;

        try {
          foreach(idx, ref arg; arguments) {
            // populate arguments, with user data if available,
            // default if not, and throw if no argument provided.
            if(idx < args.length)
              arg = to!(typeof(arg))(args[idx]);
            else static if(!is(defaultArguments[idx] == void))
              arg = defaultArguments[idx];
            else
              throw new Exception("Required argument " ~ argumentNames[idx] ~ " is missing.");
          }

          string result;

          // We have to check the return type for void
          // since it is impossible to convert a void return
          // to a printable string.
          static if(is(ReturnType!member == void))
            member(arguments);
          else
            result = to!string(member(arguments));

          writeln(result); // show the result to the user
          return 0;
        } catch(Exception e) {
          // print out the error succinctly
          stderr.writefln("%s: %s", typeid(e).name, e.msg);
          return 1;
        }
      }
    }
  }
  return 0;
}

int main(string[] args) {
  return runWithArgs(args[1 .. $]);
}

