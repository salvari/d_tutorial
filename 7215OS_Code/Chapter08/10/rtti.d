/// stores the information
immutable MoreInfo[TypeInfo] moreTypeInfo;

/// Gets extended info from a value
public immutable(MoreInfo) extTypeInfo(T)(T t) {
  if(auto ptr = typeid(t) in moreTypeInfo)
    return *ptr;
  return null;
}

/// Gets extended info from a type
public immutable(MoreInfo) extTypeInfo(T)() {
  if(auto ptr = typeid(T) in moreTypeInfo)
    return *ptr;
  return null;
}

/// The interface we provide to more info
public interface MoreInfo {
    immutable:
  /// is it an integral type?
  bool isIntegral();

  /// given a pointer to this type, return it as a string
  /// Be sure the pointer is actually the correct type!
  string getAsString(in void*);
}

/// The implementation of the interface for any type
private class MoreInfoImpl(T) : MoreInfo {
    immutable:
  // std.traits offers reflection helpers
  import trait = std.traits;

  bool isIntegral() {
    return trait.isIntegral!T;
  }

  string getAsString(in void* ptr) {
    import std.conv;
    auto t = cast(T*) ptr;
    return to!string(*t);
  }
}

/// This creates all the instances we want to enable
public mixin template EnableMoreInfo(types...) {
  /// A static constructor is run at thread startup
  shared static this() {
    foreach(type; types)
      moreTypeInfo[typeid(type)] = new immutable MoreInfoImpl!type();
  }
}

/* User code */

class A { } // a random custom class

// enable more info for built-in ints and our class
mixin EnableMoreInfo!(int, A);

void main() {
  import std.stdio;
  // test
  writeln(extTypeInfo(1).isIntegral()); // true
  writeln(extTypeInfo!(A).isIntegral()); // false

  int a = 34;
  writeln(extTypeInfo(a).getAsString(&a)); // prints 34
}

