struct Number(T) if(__traits(isArithmetic, T)) {
  T value;

  this(T t) { value = t; }

  this(N)(N n) if(isSomeNumber!N) {
    this.value = cast(T) n.value;
  }

  auto opBinary(string op, N: Number!T, T)(N rhs) {
    return(number(mixin("this.value " ~ op ~ " rhs.value")));
  }
}

// convenience constructor
auto number(T)(T t) { return Number!T(t); }

template isSomeNumber(N) {
  // what's the correct implementation?
  enum isSomeNumber = !__traits(isArithmetic, N);
}

void main() {
  int b1 = 1;
  double b2 = 2.5;
  static assert(is(typeof(b1 + b2) == double));

  Number!int n1 = number(1);
  Number!double n2 = number(2.5);
  Number!double n3 = n1 + n2; // how do we make this work?
}

template isSomeNumber(N) {
  enum isSomeNumber = is(N : Number!T, T);
}


