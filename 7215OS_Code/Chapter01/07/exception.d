class MyException : Exception {
    this(string message, string file = __FILE__, size_t line = __LINE__, Throwable next = null) {
          super(message, file, line, next);
    }
}

void main() {
    import std.stdio;
    try
      throw new MyException("message here");
    catch(MyException e)
      writeln("caught ", e);
}

