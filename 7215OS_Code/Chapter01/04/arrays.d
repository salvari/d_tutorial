void main() {
  int[] arr;
  arr ~= 1;
  arr ~= [2, 3];
  int sum(in int[] data) {
    int total = 0;
    foreach(item; data)
      total += item;
    return total;
  }

  // Dynamic arrays can be passed directly. Static
  // arrays can be sliced with the [] operator..
  import std.stdio;
  writeln("The sum of ", arr, " is ", sum(arr));

}
