interface Expression {
    // this is the common method from the classes we made
    int evaluate();
}
mixin template BinaryExpression() {
    // this is the common implementation code from the classes
    private int a, b;
    this(int left, int right) { this.a = left; this.b= right; }
}
// printResult can evaluate and print any expression class
// thanks to taking the general interface
void printResult(Expression expression) {
    import std.stdio;
    writeln(expression.evaluate());
}
class AddExpression : Expression { // inherit from the interface
    mixin BinaryExpression!(); // adds the shared code
    int evaluate() { return a + b; } // implement the method
}
class SubtractExpression : Expression {
    mixin BinaryExpression!();
    int evaluate() { return a - b; }
}
class BrokenAddExpression : AddExpression {
    this(int left, int right) {
        super(left, right);
    }
    // this changes evaluate to subtract instead of add!
    // note the override keyword
    override int evaluate() { return a - b; }
} 


void main() {
	auto add = new AddExpression(1, 2);
	printResult(add);
	auto subtract = new SubtractExpression(2, 1);
	printResult(subtract); // same function as above!
	add = new BrokenAddExpression(1, 2);
	printResult(add); // prints -1
}
