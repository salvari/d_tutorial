struct MyArray(T, size_t expectedSize = 0) {
        private T[expectedSize] staticBuffer;
        private T[] activeBuffer;
        private size_t currentLength;

        /+
        // if you want to use malloc instead of gc

        // don't copy this; we own our own memory
        // note that if you slice this, that is a borrowed reference;
        // don't store it.
        @disable this(this); // disable copying

        ~this() {
               // frees the memory if it was malloced
                if(activeBuffer.ptr !is staticBuffer.ptr)
                        free(activeBuffer.ptr);
        }
        +/

        public void reserve(size_t s) {
                if(s > activeBuffer.length)
                        activeBuffer.reserve(s); // GC operation. alternatively, you could use realloc
        }

        public @property size_t length() {
                return currentLength;
        }

        public @property void length(size_t newLength) {
                // make sure we're using the static buffer first
                if(activeBuffer is null)
                        activeBuffer = staticBuffer[];

                // then if we don't have enough room, we'll reallocate with
                // the GC (its own capacity should optimize this)
                if(newLength > activeBuffer.length) {
                        activeBuffer.length = newLength; // GC operation
                        // alternatively, you could use realloc
                }

                if(newLength < currentLength)
                        activeBuffer[newLength .. currentLength] = T.init;

                currentLength = newLength;
        }

        public void clear() { length = 0; }

        // disable concatenation
        @disable void opBinary(string op : "~")(T t){}
        @disable void opBinary(string op : "~")(T[] t){}

        // but customize appending
        public T opOpAssign(string op : "~")(T t) {
                length = currentLength + 1;
                return activeBuffer[currentLength - 1] = t;
        }

        public T[] opOpAssign(string op: "~")(T[] t) {
                auto start = currentLength;
                length = start + t.length;
                return activeBuffer[start .. start + t.length] = t[];
        }

        public T[] opSlice() {
                return activeBuffer[0 .. currentLength];
        }
        alias opSlice this;
}
// test usage
void main() { 
    // declare your array of ints with an optimized size of ten
    MyArray!(int, 10); intArray; 
    myArray ~= 1; // populate it
    myArray.length = 3; // change the length
    import std.stdio;
    writeln(myArray[]); // print the current contents
}

