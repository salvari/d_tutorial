struct RefCountedObject {
        private struct Implementation {
                /* other contents here */

                int refcount;
        }

        Implementation* data;

        alias data this;

        static RefCountedObject create() {
                RefCountedObject o = void;
                o.data = new Implementation();
                o.data.refcount = 1;
                writeln("Created."); // so we can see it in action
                return o;
        }

        this(this) {
                if(data is null) return;
                data.refcount++;
                writeln("Copied.   Refcount = ", data.refcount);
        }

        ~this() {
                if(data is null) return;
                data.refcount--;
                writeln("Released. Refcount = ", data.refcount);
                if(data.refcount == 0)
                        writeln("Destroyed.");
        }
}

