import core.stdc.stdlib;
struct UniqueArray(T) {
    private T[] payload;
    @disable this(this);

    private this(T[] t) {
        payload = t;
    }

    this(size_t count) {
        payload = (cast(T*) malloc(count * T.sizeof))[0 .. count];
    }

    UniqueArray!T release() {
        auto p = this.payload;
        this.payload = null;
        return UniqueArray!T(p);
    }

    ~this() {
        if(payload !is null)
            free(payload.ptr);
    }

    auto opDispatch(string method)() {
        return mixin("payload." ~ method);
    }
    T opIndex(size_t idx) { return payload[idx]; }
    T opIndexAssign(T t, size_t idx) { return payload[idx] = t; }
    // You may also choose to implement other operators
}
// testing use
void main() {
    auto ptr = UniqueArray!int(5);
    // use release since plain assign will fail to compile
    auto ptr2 = ptr.release; 
    import std.stdio;
    // ensure the first reference is indeed null
    writeln(ptr.ptr is null);
    // and that the second reference is correct
    writeln(ptr2.length);
 }

