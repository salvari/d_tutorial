struct NotNull(T) {
    private T notNullPayload;
    @disable this();
    private this(T t) {
        assert(t !is null);
        notNullPayload = t;
    }
    inout(T) getNotNullPayload() inout {
        return notNullPayload;
    }
    alias getNotNullPayload this;
}

struct CheckNull(T) {
    private T payload;
    bool opCast(T : bool)() { return payload !is null; }
    inout(NotNull!T) getNotNullPayload() inout {
        return NotNull!T(cast(T) payload);
    }
    alias getNotNullPayload this;
}

CheckNull!T checkNull(T)(T t) {
    return CheckNull!T(t);
}

// test usage in a function
void test(NotNull!(int*) nn) {
    import std.stdio;
    writeln(*nn, " was not null");
}

void main() {
    import std.stdio;
    int item;
    int* ptr = &item;

    // how to do the test, also try if ptr is null
    if(auto nn = ptr.checkNull) {
        test(nn);
    } else {
        writeln("Null");
    }
}

