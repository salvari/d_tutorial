void main() {
{
    // performing a SQL transaction
    database.query("START TRANSACTION");
    scope(success) database.query("COMMIT");
    scope(failure) database.query("ROLLBACK");
    database.query(step_one); // these should throw on failure
    database.query(step_two);
}
{
    // performing a multi-part file download
    auto connection = Connection.acquire();
    scope(exit) connection.close(); // release code immediately after acquisition
    connection.download("file1");
    scope(failure) std.file.remove("file1"); // undo code immediately after perform code
    connection.download("file2");
    scope(failure) std.file.remove("file2");
}

}
