import std.conv;
import core.stdc.stdlib;

class Example {}

void main() {
	auto size = __traits(classInstanceSize, Example);
	auto memory = malloc(size)[0 .. size];
	scope(exit) free(memory);
	auto obj = emplace!Example(memory);
	scope(exit) .destroy(obj);
}
