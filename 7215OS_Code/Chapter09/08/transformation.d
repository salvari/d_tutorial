// transformation function
string unCamelCase(string a) {
  import std.string;
  string ret;
  foreach(c; a)
    if((c >= 'A' && c <= 'Z'))
      ret ~= "-" ~ toLower("" ~ c);
    else
      ret ~= c;
  return ret;
}

private struct Style {
  // pointer to the associative array we’ll modify
  string[string]* aa;
  this(string[string]* aa) {
    this.aa = aa;
  }

  // getter
  @property string opDispatch(string name)() if(name != "popFront") {
    enum n = unCamelCase(name); // step 3
    return (*aa)[n];
  }

  // setter
  @property string opDispatch(string name)(string value) if(name != "popFront") {
    enum n = unCamelCase(name); // step 3
    return (*aa)[n] = value;
  }
}

class DomElement {
  string[string] styles;
  // step 6
  @property Style style() {
    return Style(&styles);
  }
}

void main() {
  auto element = new DomElement();
  element.style.backgroundColor = "red";
  element.style.fontSize = "1em";

  import std.stdio;
  writeln(element.styles);
  writeln(element.style.backgroundColor);
}

