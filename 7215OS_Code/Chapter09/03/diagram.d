enum diagramString = `
+------------------+
|LEN | ID | MSG    |
+------------------+
`;

struct DiagramField {
  string name;
  int length;
}

DiagramField[] readDiagram(string diagram) {
  DiagramField[] fields;
  import std.string;
  auto lines = diagram.splitLines();
  foreach(line; lines) {
    if(line.length == 0) continue; // blank line
    if(line[0] == '+') continue; // separator line

    auto parts = line.split("|");
    foreach(part; parts) {
      if(part.length == 0) continue;
      DiagramField field;
      field.name = part.strip;
      field.length = part.length / 4;
      fields ~= field;
    }
  }

  return fields;
}

string toStructDefinition(DiagramField[] fields) {
  string code = "struct {\n";

  foreach(field; fields) {
    string type;
    switch(field.length) {
      case 1: type = "ubyte"; break;
      case 2: type = "ushort"; break;
      case 4: type = "uint"; break;
      case 8: type = "ulong"; break;
      default: assert(0);
    }
    code ~= "\t" ~ type ~ " " ~ field.name ~ ";\n";
  }

  code ~= "\n}";
  return code;
}

struct StructFromDiagram(string diagram) {
  mixin(toStructDefinition(readDiagram(diagram)));
}

alias Message = StructFromDiagram!diagramString;

/*
// an alternative way to form to the struct
struct Message {
  mixin(toStructDefinition(readDiagram(diagramString)));
}
*/

void main() {
  import std.stdio;
  debug writeln(toStructDefinition(readDiagram(diagramString)));
  Message m;
  m.ID = 5;
  writeln(m);
}

