void callFunction(string functionName) {
  s: switch(functionName) {
     default: assert(0); // can add defaults or cases outside the loop
    foreach(methodName; __traits(allMembers, mixin(__MODULE__))) {
       case methodName:
           // implement the call
       break s; // break the switch specifically to clarify intent inside loop
    }
  }
}
