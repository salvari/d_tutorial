import std.stdio, std.string;
string[] parse(string code) {
  return code.split(" ");
}

string convertToD(string[] pieces) {
  string code;

  foreach(piece; pieces) {
    if(isNumeric(piece)) {
      code ~= "push(" ~ piece ~ ");\n";
    } else {
      code ~= "push(call!`"~piece~"`(pop(), pop()));\n";
    }
  }

  return code;
}

void runDslCode(string code)() {
  int[16] stack;
  int stackPosition;
  void push(int p) {
    stack[stackPosition++] = p;
  }

  int pop() {
    return stack[--stackPosition];
  }

  int call(string op)(int a, int b) {
    return mixin("b"~op~"a");
  }

  mixin(convertToD(parse(code)));

  writeln(stack[0 .. stackPosition]);
}

void main() {
  enum code = "5 5 + 3 - 2 * 1 + 3 /";
  // writeln(parse(code)); // to aid with debugging
  // writeln(convertToD(parse(code)); // debugging aid
  // usage:
  runDslCode!"5 5 + 3 - 2 * 1 + 3 /"();
}

