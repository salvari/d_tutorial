void foo(string variation)() {
        import std.stdio;
        static if(variation == "test")
                writeln("test variation called");
        else
                writeln("other variation called");
}

alias testVariation = foo!"test";
alias otherVariation = foo!"";

void main() {
        testVariation();
        otherVariation();
}

