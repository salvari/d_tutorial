import std.traits;

// this alias represents the unified type our wrapper yields
alias WrapperFunctionType = string delegate(string[]);

// step 2: this returns the wrapped function, given the original
WrapperFunctionType wrap(Func)(Func func) if(isDelegate!Func) {
        // it immediately returns the wrapped function here
        return delegate string(string[] args) {
                import std.conv;
                ParameterTypeTuple!Func funcArgs;

                // step 3: populating the arguments
                foreach(idx, ref arg; funcArgs) {
                        if(idx >= args.length) break;
                        // the cast allows us to write to const
                        // arguments. to!() performs conversions.
                        cast() arg = to!(typeof(arg))(args[idx]);
                }

                // step 4: call the function, handling the
                // return value.
                string ret;
                static if(is(ReturnType!func == void))
                        func(funcArgs);
                else
                        ret = to!(typeof(return))(func(funcArgs));
                return ret;
        }; // remember, this is a return statement, needing ;
}

WrapperFunctionType[string] wrapAll(T)(ref T obj) {
        WrapperFunctionType[string] wrappers;

        foreach(memberName; __traits(allMembers, T)) {
                // only wrapping methods
                static if(is(typeof(__traits(getMember, T, memberName)) == function))
                        // wrap with a delegate from obj
                        wrappers[memberName] = wrap(&__traits(getMember, obj, memberName));
        }

        return wrappers;
}

// a test structure with various method types
struct Test {
        int a;
        void foo() {
                import std.stdio; writeln("foo called");
        }

        int setA(int a) { return this.a = a; }
        int getA() { return this.a; }
}


void main() {
        Test t;
        // Wrap functions into a uniform array with string keys
        auto functions = wrapAll(t);

        // call foo with no arguments
        functions["foo"](null);

        functions["setA"](["10"]); // calls t.setA(10);
        import std.stdio;
        // confirm we can get the value and that it matches
        // the original object too.
        writeln(functions["getA"](null), " == ", t.getA());
}

