interface Animal {
  void speak();
  void speak(string);
}

struct Duck {
  void speak() {
    import std.stdio;
    writeln("Quack!");
  }
  void speak(string whatToSay) {
    import std.stdio;
    writeln("Quack! ", whatToSay);
  }
}

void callSpeak(Animal a) {
  a.speak();
  a.speak("hello");
}

void main() {
  Duck d;
  // callSpeak(wrap!Animal(&d)); // we want to make this line work
}

