class A {}
class B : A {}
class C : A {}

import std.stdio;

void test(Object b, Object c) { writeln("Object, Object"); }
void test(A b, C c) { writeln("A, C"); }
B test(B b, C c) { writeln("B, C"); return b; }
void test(B b, B c) { writeln("B, B"); }
void test(C b, C c) { writeln("C, C"); }

/*
  The goal is to call the overload of func that best
  matches the *dynamic types* of the passed argument.
*/
auto dynamicDispatch(alias func, T...)(T t) {
  import std.traits;
  import std.typetuple;

  // step 1: getting all overloads of the given function
  alias overloads = TypeTuple!( /* TypeTuple is like our alias helper but it works with multiple arguments and is found in the standard library module std.typetuple */
    __traits(getOverloads, // get all overloads…
      __traits(parent, func), // from the parent of the function…
      __traits(identifier, func))); // that share a name with it.

  // step 2: ensure we weren’t given null
  foreach(a; t)
    assert(a !is null);

  // step 3: the variable that will hold our match
  CommonType!(staticMap!(ReturnType, overloads)) delegate() dg;
  int bestSpecificity = int.min; // and the best match’s score

  overloads:
  foreach(overload; overloads) {
    // step 4: loop over each one and find a match
    ParameterTypeTuple!overload args;
    static if(T.length == args.length) {
      int specificity = 0;
      bool isExactMatch = true;
      foreach(idx, ref arg; args) {
        arg = cast(typeof(arg)) t[idx];
        if(arg is null)
          continue overloads;
        // We check for exact match – where the typeid we have
        // is a perfect match for the typeid we need
        if(isExactMatch && typeid(typeof(arg)) !istypeid(t[idx]))
          isExactMatch = false;
        // The specificity is the distance from Object; the number
        // of base classes. We multiply by the argument length to
        // help get an average of all arguments.
        specificity += BaseClassesTuple!(typeof(arg)).length * args.length;
      }

      specificity /= args.length; // average specificity
      // Debugging info, printing out the options we found
      writeln("specificity of ", typeof(overload).stringof, " = ", specificity);

      if(specificity > bestSpecificity) {
        // If it is a better match than we have, it becomes the
        // new best
        bestSpecificity = specificity;
        // the cast ensures we get a consistent type
        dg = { return cast(ReturnType!dg) overload(args); };

        // found an exact match, no point continuing to search
        if(isExactMatch)
          break overloads;
      }
    }
  }

  // if a suitable function was found, call it
  if(dg)
    return dg();

  // otherwise, throw an exception
  throw new Exception("no dynamic match found");
}

void main() {
  Object a = new A();
  Object b = new B();
  dynamicDispatch!test(a, b);
}

