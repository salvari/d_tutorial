void main() {
  clearScreen();
  //print("Hello, world, from bare metal!");
  initializeSystem();

  while(true) {
    asm {
      sti; // enable interrupts
      hlt; // halt the CPU until an interrupt arrives
      cli; // disable interrupts while we handle the data
    }

    if(gotKey) {
      print("We got a key!");
      // Change the color of the W so we can see a
      // difference on each key press
      ubyte* videoMemory = cast(ubyte*) 0xb8000;
      videoMemory[1] += 1;

      // clear out the buffer
      gotKey = 0;
    }
  }
}


void clearScreen() {
        ubyte* videoMemory = cast(ubyte*) 0xb8000;
        foreach(idx; 0 .. 80*25) {
                videoMemory[idx * 2] = ' ';
                videoMemory[idx * 2 + 1] = 0b111;
        }

}

void print(string msg) {
        ubyte* videoMemory = cast(ubyte*) 0xb8000;
        foreach(char c; msg) {
                *videoMemory = c;
                videoMemory += 2;
        }
}

// The following values are defined by the hardware specs.

enum PIC1 = 0x20; /* IO port for master PIC */
enum PIC2 = 0xA0; /* IO port for slave PIC */
enum PIC1_COMMAND = PIC1; /* Commands to PIC1 */
enum PIC1_DATA = (PIC1+1); /* IO port for data to PIC1 */
enum PIC2_COMMAND = PIC2; // ditto for PIC2
enum PIC2_DATA = (PIC2+1);

// This is the interrupt number where the IRQs are mapped.
// 8 is the default set by the system at boot up. We can
// also change this by reprogramming the interrupt controller,
// which is recommended to avoid conflicts in real world code,
// but here, for simplicity, we'll just use the default.
enum IRQOffset = 8;

// This is a data structure defined by the hardware to hold
// an interrupt handler.
align(1)
struct IdtEntry {
  align(1):
  ushort offsetLowWord;
  ushort selector;
  ubyte reserved;
  ubyte attributes;
  ushort offsetHighWord;
}

// This is a hardware-defined structure to store the location of
// a descriptor table.
align(1)
struct DtLocation {
  align(1):
  ushort size;
  void* address;
}

// Ensure the hardware structure is sized correctly.
static assert(DtLocation.sizeof == 6);

// Hardware defined data structure for holding
// the Global Descriptor Table which holds
// memory protection information.
align(1)
struct GdtEntry {
  align(1):
  ushort limit;
  ushort baseLow;
  ubyte baseMiddle;
  ubyte access;
  ubyte limitAndFlags;
  ubyte baseHigh;
}
/* End of hardware data definitions */

// This is an interrupt handler that simply returns.
void nullIh() {
  asm {
    naked;
    iretd;
  }
}

// stores the last key we got
__gshared uint gotKey = 0;

// This is a keyboard interrupt handler
void keyboardHandler() {
  asm {
    naked; // we need complete control
    push EAX; // store our scratch register

    xor EAX, EAX; // clear the scratch register
    in AL, 0x60; // read the keyboard byte

    mov [gotKey], EAX; // store what we got for use later

    // acknowledge that we handled the interrupt
    mov AL, 0x20;
    out 0x20, AL;

    pop EAX; // restore our scratch register
    iretd; // required special return instruction
  }
}

// Now, we define some data buffers to hold our tables
// that the hardware will use.

// This global array holds our entire interrupt handler table.
__gshared IdtEntry[256] idt;
__gshared GdtEntry[4] gdt; // memory protection tables
__gshared ubyte[0x64] tss; // task state data

// Now, we'll declare helper functions to load this data.

/// Enables interrupt requests.
/// lowest bit set = irq 0 enabled
void enableIrqs(ushort enabled) {
  // the hardware actually expects a disable bitmask
  // rather than an enabled list/ so we'll flip the bits.
  enabled = ~enabled;
  asm {
    // tell the interrupt controller to unmask the interrupt requests, enabling them.
    mov AX, enabled;
    out PIC1_DATA, AL;
    mov AL, AH;
    out PIC2_DATA, AL;
  }
}

// The interrupt handler structure has a strange layout
// due to backward compatibility requirements on the
// processor. This helper function helps us set it from
// a regular pointer.
void setInterruptHandler(ubyte number, void* handler) {
  IdtEntry entry;
  entry.offsetLowWord = cast(size_t)handler & 0xffff;
  entry.offsetHighWord = cast(size_t)handler >> 16;
  entry.attributes = 0x80 /* present */ |  0b1110 /* 32 bit interrupt gate */;
  entry.selector = 8;
  idt[number] = entry;
}

// Tell the processor to load our new table.
void loadIdt() {
  DtLocation loc;

  static assert(idt.sizeof == 8 * 256);

  loc.size = cast(ushort) (idt.sizeof - 1);
  loc.address = &idt;

  asm {
    lidt [loc];
  }
}

// Load a global descriptor table
//
// We don't actually care about memory protection or
// virtual memory, so we load a simple table here that
// covers the entire address space for both code and data.
void loadGdt() {
  GdtEntry entry;
  entry.limit = 0xffff;

  // page granularity and 32 bit
  entry.limitAndFlags = 0xc0 | 0x0f;
  entry.access = 0x9a; // present kernel code
  gdt[1] = entry;
  entry.access = 0x92; // data
  gdt[2] = entry;

  auto tssPtr = cast(size_t) &tss;
  entry.baseLow = tssPtr & 0xffff;
  entry.baseMiddle = (tssPtr >> 16) & 0xff;
  entry.baseHigh = tssPtr >> 24;
  entry.limit = tss.sizeof;
  entry.limitAndFlags = 0x40; // present

  entry.access = 0x89; // a task buffer (we don't use it but do prepare it)
  gdt[3] = entry;

  DtLocation loc;
  loc.size = cast(ushort) (gdt.sizeof - 1);
  loc.address = &gdt;

  asm {
    lgdt [loc]; // tell the processor to load it
  }
}


// This function initializes the system and enables interrupts,
// setting all handlers, except the keyboard IRQ, to our null
// handler. We'll call it from main.
void initializeSystem() {
  loadGdt();

  foreach(i; 0 .. 256)
    setInterruptHandler(cast(ubyte) i, &nullIh);

  setInterruptHandler(IRQOffset + 1, &keyboardHandler);
  loadIdt();

  enableIrqs(1 << 1 /* IRQ1 enabled: keyboard */);

  asm {
    sti; // enable interrupts
  }
}

