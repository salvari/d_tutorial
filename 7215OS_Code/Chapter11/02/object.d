module object;

static assert((void*).sizeof == 4); // must be 32 bit

alias string = immutable(char)[];
alias size_t = uint;
alias ptrdiff_t = int;

extern(C)
void _d_run_main() {
        asm {
                naked;
                call _Dmain;
                loop_forever:
                        cli;
                        hlt;
                        jmp loop_forever;
        }
}

extern(C) void _d_dso_registry() {}
extern(C) __gshared void* _Dmodule_ref;
extern(C) void _Dmain();

// required support classes by the compiler

class Object { }

class Throwable {}
class Exception : Throwable {}
class Error : Throwable {}

class TypeInfo { }

class TypeInfo_Struct : TypeInfo {
        void*[13] compilerProvidedData;
}

class TypeInfo_Interface : TypeInfo {
        TypeInfo_Class info;
}

class TypeInfo_Class : TypeInfo {
        void*[17] compilerProvidedData;
}

struct ModuleInfo {}

