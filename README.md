D programming language
======================

### Tutorial y notas

dub
===

Dub

Básicos
=======

writeln y write
---------------

**write** y **writeln** son funciones del modulo **std.stdio**

Las dos admiten como parámetros uno o varios *string*

``` d
import std.stdio;

void main()
{
    writeln("Hello world!", "Hello fish!");
}
```

Salida formateada
-----------------

Compilador
==========

DMD
---

Instalado desde la página oficial [compilador](http://dlang.org/download.html)

``` bash
dmd files -switches
```

| Extensión | Tipo de fichero            |
|-----------|----------------------------|
| none      | Fichero fuente en D        |
| .d        | Fichero fuente en D        |
| .dd       | Fichero Ddoc               |
| .di       | Fichero de interface       |
| .o        | Código objeto              |
| .a        | Librerías en código objeto |

### Switches

@cmdfile  
Si *cmdfile* es una variable de entorno leer las opciones de compilación desde la variable. Si no, leer las opciones desde un fichero llamado *cmdfile*

-c  
Solo compilar, no enlazar

-color=on|off  
Mostrar salida con colores por consola

-cov  
Genera información para hacer [*code coverage analysis*](http://dlang.org/code_coverage.html)

-cov=nnn  
Genera información para hacer *code coverage analysis* pero solo si cubre menos del nnn% del código ejecutable.

-D  
Genera documentación a partir del código fuente

-Dddocdir  
Escribir la documentación al directorio docdir se puede usar **-op** si hay que retener la jerarquía original de paquetes

-Dffilename  
Escribir documentación al fichero *filename*

-d  
Permitir el uso de *deprecated features* y el uso de símbolos con atributos descontinuados

-dw  
Tratar el uso de funcionalidades descontinuadas como *warnings*, esta es la opción por defecto

-de  
Tratar el uso de funcionalidades y atributos descontinuados como errores

-debug  
Compilar en modo depuración

-debug=level  
Compilar en modo depuración con nivel de depuración \<= level

-debug=ident  
Compilar en modo depuración el identificador ident

-debuglib=libname  
Enlazar *libname* como la librería por defecto al compilar para depuración simbólica en lugar de *libphobos2.a* Si no se provee un nombre *libname* no se enlaza ninguna biblioteca por defecto

-defaultlib=libname  
Enlazar la biblioteca *libname* como la biblioteca por defecto cuando no se compile para depuración simbólica en lugar de *libphobos2.a.* Si no se provee un *libname* no se enlaza ninguna librería por defecto

-deps=filename  
Escribir las dependencias del módulo como texto en el fichero *filename*

-fPIC

: Generar Position Independent Code (que se usar para construir librerías compartidas)

-g  
add Dwarf symbolic debug info with D extensions for debuggers such as ZeroBUGS

-gc  
add Dwarf symbolic debug info in C format for debuggers such as gdb

-gs  
always generate standard stack frame

-H  
Generar un fichero de interface en D

-Hddir  
write D interface file to dir directory. -op can be used if the original package hierarchy should be retained

-Hffilename  
write D interface file to filename

--help  
print brief help to console

-Ipath  
Donde buscar los ficheros para importar el parámetro *path* es una lista de directorios separados por ; Se pueden pasar múltiples **-I** los directorios se exploran en el orden en que se pasan como parámetro

-ignore  
ignorar los pragma no soportados

-inline  
Dejar que el compilador aplique expansión *inline* de las funciones Esto puede mejorar el rendimiento pero dificultando el uso de un depurador

-Jpath  
Donde buscar ficheros para importar expresiones Esta opción es imprescindible para usar *ImportExpressions*. path es una lista de directorios separados por ; Se pueden usar múltiples **-J** y los directorios serán explorados en el orden en que se especifican.

-Llinkerflag  
pasar opción *linkerflag* al linker, por ejemplo: **-L-M**

-lib  
Generar una fichero de biblioteca como salida en lugar de un fichero de código objeto. Todos los ficheros fuentes compilados así como los ficheros de código objeto y los ficheros de biblioteca especificados en la linea de comandos se insertan en la libreria de salida. Los módulos fuente compilados pueden ser partidos en varios módulos objeto para mejorar la granularidad. El nombre de la biblioteca se toma del nombre del primero módulo fuente en ser compilado. El nombre puede ser especificado explícitamente con la opcion **-of**

-m32  
compile a 32 bit executable. This is the default for the 32 bit dmd.

-m64  
compile a 64 bit executable. This is the default for the 64 bit dmd.

-main  
Add a default main() function when compiling. This is useful when unittesting a library, as it enables the user to run the unittests in a library without having to manually define an entry-point function.

-man  
open browser specified by the BROWSER environment variable on this page. If BROWSER is undefined, x-www-browser is assumed.

-map  
generate a .map file

-boundscheck=[on|safeonly|off]  
controls if bounds checking is enabled.

-   on: Bounds checks are enabled for all code. This is the default. safeonly: Bounds checks are enabled only in @safe code. This is the default for -release builds.

-   off: Bounds checks are disabled completely (even in @safe code). This option should be used with caution and as a last resort to improve performance. Confirm turning off @safe bounds checks is worthwhile by benchmarking.

-noboundscheck  
turns off all array bounds checking, even for safe functions. Deprecated (use -boundscheck=off instead).

-O  
Optimize generated code. For fastest executables, compile with the -O -release -inline -boundscheck=off switches together.

-o-  
Suppress generation of object file. Useful in conjuction with -D or -H flags.

-odobjdir  
write object files relative to directory objdir instead of to the current directory. -op can be used if the original package hierarchy should be retained

-offilename  
Set output file name to filename in the output directory. The output file can be an object file, executable file, or library file depending on the other switches.

-op  
normally the path for .d source files is stripped off when generating an object, interface, or Ddoc file name. -op will leave it on.

-profile  
profile the runtime performance of the generated code

-property  
enforce use of @property on property functions

-quiet  
suppress non-essential compiler messages

-release  
compile release version, which means not emitting run-time checks for contracts and asserts. Array bounds checking is not done for system and trusted functions, and assertion failures are undefined behaviour.

-run srcfile args... compile  
link, and run the program srcfile with the rest of the command line, args..., as the arguments to the program. No .o or executable file is left behind.

-shared  
generate shared library

-unittest  
compile in unittest code, turns on asserts, and sets the unittest version identifier

-v  
verbose

-version=level  
compile in version level \>= level

-version=ident  
compile in version identifier ident

-vtls  
print informational messages identifying variables defaulting to thread local storage. Handy for migrating to shared model.

-w  
enable warnings

-wi  
enable informational warnings (i.e. compilation still proceeds normally)

-X  
generate JSON file

-Xffilename  
write JSON file to filename


